<?php

namespace App\Http\Controllers;

use App\Models\Deposit;
use Illuminate\Http\Request;
use Session;

class DepositController extends Controller
{
    public function depositInsert(Request $request)
    {
//        dd($request->all());

        $request->validate([
            'amount' => 'required|numeric|gt:0',
            'method_code' => 'required',
        ]);


        $user = auth()->user();
        $data = new Deposit();
        $data->user_id = $user->id;
        $data->method_code =$request->method_code;
        $data->method_currency = strtoupper($request->method_code);
        $data->amount = $request->amount;
        $data->charge = 0;
        $data->rate = 0;
        $data->final_amo = $request->amount;
        $data->btc_amo = 0;
        $data->btc_wallet = "";
        $data->trx = getTrx();
        $data->try = 0;
        $data->status = 0;
        $data->save();
        session()->put('Track', $data['trx']);
        return redirect('/deposit/preview');
    }


    public function depositPreview()
    {

        $track = session()->get('Track');
        $data = Deposit::where('trx', $track)->orderBy('id', 'DESC')->firstOrFail();

        if (is_null($data)) {
            $notify[] = ['error', 'Invalid Deposit Request'];
            return redirect()->route(gatewayRedirectUrl())->withNotify($notify);
        }
        if ($data->status != 0) {
            $notify[] = ['error', 'Invalid Deposit Request'];
            return redirect()->route(gatewayRedirectUrl())->withNotify($notify);
        }
        $pageTitle = 'Payment Preview';
        return view('user.deposit_preview', compact('data', 'pageTitle'));
    }

}
