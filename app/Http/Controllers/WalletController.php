<?php

namespace App\Http\Controllers;

use App\Models\Wallet;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{

    public function wallet()
    {
        $pageTitle = "Staking wallet";
        $user = Auth::user();
        $wallets = Wallet::where('user_id', $user->id)->get();

        return view('user.wallet', compact('pageTitle','wallets'));
    }

}
