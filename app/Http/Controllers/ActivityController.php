<?php

namespace App\Http\Controllers;

class ActivityController extends Controller
{

    public function activity()
    {
        $pageTitle = "Activity";

        return view('user.activity', compact('pageTitle'));
    }

}
