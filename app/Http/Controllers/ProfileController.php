<?php

namespace App\Http\Controllers;

class ProfileController extends Controller
{

    public function profile()
    {
        $pageTitle = "Profile";

        return view('user.profile', compact('pageTitle'));
    }

}
