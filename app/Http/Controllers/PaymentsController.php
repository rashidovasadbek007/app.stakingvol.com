<?php

namespace App\Http\Controllers;

class PaymentsController extends Controller
{

    public function payments()
    {
        $pageTitle = "Payments";

        return view('user.payments', compact('pageTitle'));
    }
    public function payment_bill()
    {
        $pageTitle = "Payments";

        return view('pages.payment_bill', compact('pageTitle'));
    }

}
