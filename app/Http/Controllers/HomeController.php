<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use App\Models\UserStaking;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function home()
    {
        $pageTitle = "Welcome";
        $user = Auth::user();
        $userstakings = UserStaking::where('user_id', $user->id)->get();
        $transactions = Transaction::where('user_id', $user->id)->where('user_staking_date', '!=', null)->orderby('user_staking_date', 'desc')->limit(10)->get();
        return view('home', compact('pageTitle', 'transactions','userstakings'));
    }


}
