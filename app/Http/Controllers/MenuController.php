<?php

namespace App\Http\Controllers;

use App\Models\Wallet;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    public function menu_sidebar()
    {

        return view('includes.menu_sidebar');
    }
    public function menu_transfer()
    {

        return view('includes.menu_transfer');
    }
    public function menu_request()
    {

        return view('includes.menu_request');
    }
    public function menu_exchange()
    {

        return view('includes.menu_exchange');
    }
    public function menu_notifications()
    {

        return view('includes.menu_notification');
    }
    public function menu_card_settings()
    {

        return view('includes.menu_card_settings');
    }
}
