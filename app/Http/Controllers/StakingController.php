<?php

namespace App\Http\Controllers;

use App\Models\UserStaking;
use Illuminate\Support\Facades\Auth;

class StakingController extends Controller
{

    public function staking()
    {
        $pageTitle = "Stakings";
        $user = Auth::user();
        $userstakings = UserStaking::where('user_id', $user->id)->get();
        return view('user.staking', compact('pageTitle','userstakings'));
    }

}
