<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $table = 'deposits';
    protected $guarded = ['id'];

    protected $casts = [
        'detail' => 'object'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function gateway()
    {
        return $this->belongsTo(Wallet::class, 'method_code', 'symbol');
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'method_currency', 'id');
    }

    // scope
    public function scopeGateway_currency()
    {
        return GatewayCurrency::where('method_code', $this->method_code)->where('currency', $this->method_currency)->first();
    }

    public function scopeBaseCurrency()
    {
        return $this->wallet ? 'USD' : $this->wallet->symbol;
    }

    public function scopePending()
    {
        return $this->where('status', 2);
    }


}
