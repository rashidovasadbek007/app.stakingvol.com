<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Wallet extends Model
{
    use HasFactory;
    protected $fillable = ['user_id'];
    protected $table = 'wallets';

    /**
     * Get the user that owns the Wallet
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    /**
     * Get the user that owns the Wallet
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency(): BelongsTo
    {
        return $this->belongsTo(CryptoCurrency::class, 'symbol', 'symbol');
    }
    public static function expressCreate($user_id,$symbol){
        $wallet=new Wallet;
        $wallet->symbol=$symbol;
        $wallet->user_id=$user_id;
        $wallet->balance=0.0;
        $wallet->save();
        return $wallet;
    }

}
