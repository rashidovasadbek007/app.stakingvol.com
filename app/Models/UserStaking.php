<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class UserStaking extends Model
{
    protected $table = 'user_stakings';
    protected $casts = [
        'start_date' => 'date',
    ];
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function staking(){
        return $this->belongsTo(Staking::class);
    }
    public function transactions(){
        return $this->hasMany(Transaction::class,'user_staking_id');
    }
    public function leftDays(){
        $toDate = now();
        $fromDate = Carbon::parse($this->start_date);
        return $toDate->diffInDays($fromDate);
    }
}
