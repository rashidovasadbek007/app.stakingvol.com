<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staking extends Model
{
    protected $staking = 'stakings';

    public function currency(){
        return $this->belongsTo(CryptoCurrency::class);
    }

}
