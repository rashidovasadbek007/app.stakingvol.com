<?php

use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\WalletController;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\MenuController;
use App\Http\Controllers\StakingController;
use App\Http\Controllers\DepositController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/login', [LoginController::class, 'login'])->name('login');

Auth::routes(['verify' => true]);
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', [HomeController::class, 'home'])->name('home');
    Route::get('/', [HomeController::class, 'home'])->name('home');
    Route::get('/logout', [LogoutController::class, 'logout']);
    Route::get('/user/profile', [ProfileController::class,'profile']);
    Route::get('/user/wallet', [WalletController::class,'wallet']);
    Route::get('/user/staking', [StakingController::class,'staking']);
    Route::get('/user/activity', [ActivityController::class,'activity']);
    Route::get('/user/payments', [PaymentsController::class,'payments']);
    Route::get('/menu-sidebar', [MenuController::class,'menu_sidebar']);
    Route::get('/menu-transfer', [MenuController::class,'menu_transfer']);
    Route::get('/menu-request', [MenuController::class,'menu_request']);
    Route::get('/menu-exchange', [MenuController::class,'menu_exchange']);
    Route::get('/payment-bill', [PaymentsController::class,'payment_bill']);
    Route::get('/menu-notifications', [MenuController::class,'menu_notifications']);
    Route::get('/menu-card-settings', [MenuController::class,'menu_card_settings']);
    Route::post('/deposit/insert', [DepositController::class,'depositInsert']);
    Route::get('deposit/preview', [DepositController::class,'depositPreview'])->name('deposit.preview');

});

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
