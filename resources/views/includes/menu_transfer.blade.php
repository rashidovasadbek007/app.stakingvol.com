
<div class="menu-size" style="height:370px;">
    <div class="d-flex mx-3 mt-3 py-1">
        <div class="align-self-center">
            <h1 class="mb-0">Deposit Funds</h1>
        </div>
        <div class="align-self-center ms-auto">
            <a href="#" class="ps-4 shadow-0 me-n2" data-bs-dismiss="offcanvas">
            <i class="bi bi-x color-red-dark font-26 line-height-xl"></i>
            </a>
        </div>
    </div>
    <form action="/deposit/insert" method="post">
        @csrf

    <div class="divider divider-margins mt-3"></div>
    <div class="content mt-0">
        <div class="form-custom form-label form-icon">
            <i class="bi bi-wallet2 font-14"></i>
            <select class="form-select rounded-xs" id="c6" aria-label="Floating label select example" name="method_code">
{{--                <option selected>Main Account</option>--}}
                @foreach(\App\Models\Wallet::where("user_id",Auth::id())->where('balance',">",0)->get() as $w)
                    <option value="{{$w->id}}">{{$w->symbol}}</option>
                @endforeach

            </select>
            <label for="c6" class="form-label-always-active color-highlight font-11">Choose Account</label>
        </div>
        <div class="pb-3"></div>
        <div class="form-custom form-label form-icon">
            <i class="bi bi-code-square font-14"></i>
            <input type="number" class="form-control rounded-xs" name="amount" id="c4" placeholder="150.00"/>
            <label for="c4" class="form-label-always-active color-highlight font-11">Amount</label>
            <span class="font-10">( Currency: USD )</span>
        </div>

    </div>
    <button data-bs-dismiss="offcanvas" class="mx-3 mb-3 btn btn-full gradient-green shadow-bg shadow-bg-s">Deposit Funds</button>
    </form>
</div>
