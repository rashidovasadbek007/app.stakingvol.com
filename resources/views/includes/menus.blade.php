
<div id="menu-sidebar"
     data-menu-active="nav-pages"
     data-menu-load="/menu-sidebar"
     class="offcanvas offcanvas-start offcanvas-detached rounded-m">
</div>

<!-- Highlights Menu -->
<div id="menu-highlights"
     data-menu-load="menu-highlights.html"
     class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
</div>

<div id="menu-highlights"
     data-menu-load="menu-highlights.html"
     class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
</div>

<!-- Notifications Bell -->
<div id="menu-notifications" data-menu-load="/menu-notifications"
     class="offcanvas offcanvas-top offcanvas-detached rounded-m">
</div>

<!-- Card Menu More -->
<div id="menu-card-more" data-menu-load="/menu-card-settings"
     class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
</div>

<!-- Transfer Button Menu -->
<div id="menu-transfer" data-menu-load="/menu-transfer"
     class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
</div>

<!-- Transfer Friends Menu -->
<div id="menu-friends-transfer" data-menu-load="menu-friends-transfer.html"
     class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
</div>

<!-- Request Button Menu -->
<div id="menu-request" data-menu-load="/menu-request"
     class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
</div>

<!-- Exchange Button Menu -->
<div id="menu-exchange" data-menu-load="/menu-exchange"
     class="offcanvas offcanvas-bottom offcanvas-detached rounded-m">
</div>

<div class="offcanvas offcanvas-bottom rounded-m offcanvas-detached" id="menu-install-pwa-ios">
    <div class="content">
        <img src="/frontend/app/icons/icon-128x128.png" alt="img" width="80" class="rounded-m mx-auto my-4">
        <h1 class="text-center">Install PayApp</h1>
        <p class="boxed-text-xl">
            Install PayApp on your home screen, and access it just like a regular app. Open your Safari menu and tap "Add to Home Screen".
        </p>
        <a href="#" class="pwa-dismiss close-menu color-theme text-uppercase font-900 opacity-50 font-11 text-center d-block mt-n2" data-bs-dismiss="offcanvas">Maybe Later</a>
    </div>
</div>

<div class="offcanvas offcanvas-bottom rounded-m offcanvas-detached" id="menu-install-pwa-android">
    <div class="content">
        <img src="app/icons/icon-128x128.png" alt="img" width="80" class="rounded-m mx-auto my-4">
        <h1 class="text-center">Install PayApp</h1>
        <p class="boxed-text-l">
            Install PayApp to your Home Screen to enjoy a unique and native experience.
        </p>
        <a href="#" class="pwa-install btn btn-m rounded-s text-uppercase font-900 gradient-highlight shadow-bg shadow-bg-s btn-full">Add to Home Screen</a><br>
        <a href="#" data-bs-dismiss="offcanvas" class="pwa-dismiss close-menu color-theme text-uppercase font-900 opacity-60 font-11 text-center d-block mt-n1">Maybe later</a>
    </div>
</div>
