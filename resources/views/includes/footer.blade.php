<div id="footer-bar" class="footer-bar-1 footer-bar-detached">
    <a href="/user/staking" class="{{ request()->is('user/staking') ? 'active-nav':'' }}"><i
            class="bi bi-wallet2"></i><span>Stake</span></a>
    <a href="/user/wallet" class="{{ request()->is('user/wallet') ? 'active-nav':'' }}"><i
            class="bi bi-graph-up"></i><span>Wallet</span></a>
    <a href="/home" class="circle-nav-2"><i class="bi bi-house-fill"></i><span>Home</span></a>
    <a href="/user/activity" class="{{ request()->is('user/activity') ? 'active-nav':'' }}"><i
            class="bi bi-receipt"></i><span>Staking active</span></a>
    <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-sidebar"><i
            class="bi bi-three-dots"></i><span>More</span></a>
</div>
