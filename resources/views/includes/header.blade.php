@php
    $user = \Illuminate\Support\Facades\Auth::user();
@endphp

<div class="pt-3 {{ request()->is('user/profile*') ? 'd-none':'' }}">
    <div class="page-title d-flex">
        <div class="align-self-center me-auto">
            <p class="color-white header-date"></p>
            <h1 class="color-white">{{$pageTitle}}</h1>
        </div>
        <div class="align-self-center ms-auto">
            <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-notifications"
               class="icon bg-white color-theme rounded-m shadow-xl">
                <i class="bi bi-bell-fill font-17"></i>
                <em class="badge bg-red-dark color-white scale-box ">3</em>
            </a>
            <a href="#" data-bs-toggle="dropdown" class="icon bg-white color-theme rounded-m shadow-xl">
                <img src="@if($user->profile_photo_path) {{$user->profile_photo_path}}   @else https://ui-avatars.com/api/?bold=true&length=1&color=ED5565&name={{$user->email}} @endif" width="45"
                     class="rounded-m" alt="img">
            </a>
            <!-- Page Title Dropdown Menu-->
            <div class="dropdown-menu">
                <div class="card card-style shadow-m mt-1 me-1">
                    <div class="list-group list-custom list-group-s list-group-flush rounded-xs px-3 py-1">
                        <a href="/user/wallet" class="list-group-item">
                            <i class="has-bg gradient-green shadow-bg shadow-bg-xs color-white rounded-xs bi bi-credit-card"></i>
                            <strong class="font-13">Wallet</strong>
                        </a>
                        <a href="/user/activity" class="list-group-item">
                            <i class="has-bg gradient-blue shadow-bg shadow-bg-xs color-white rounded-xs bi bi-graph-up"></i>
                            <strong class="font-13">Activity</strong>
                        </a>
                        <a href="/user/profile" class="list-group-item">
                            <i class="has-bg gradient-yellow shadow-bg shadow-bg-xs color-white rounded-xs bi bi-person-circle"></i>
                            <strong class="font-13">Account</strong>
                        </a>
                        <a href="/logout" class="list-group-item">
                            <i class="has-bg gradient-red shadow-bg shadow-bg-xs color-white rounded-xs bi bi-power"></i>
                            <strong class="font-13">Log Out</strong>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
