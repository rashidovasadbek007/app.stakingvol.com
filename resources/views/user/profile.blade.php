@extends('layouts.app')
@section('title', $pageTitle)


@section('content')

        <div class="notch-clear"></div>
        <div class="pt-5 mt-4"></div>
        <div class="card card-style overflow-visible mt-5">
            <div class="mt-n5"></div>
            <img src="/frontend/images/pictures/31t.jpg" alt="img" width="180" class="mx-auto rounded-circle mt-n5 shadow-l">
            <h1 class="color-theme text-center font-30 pt-3 mb-0">{{auth()->user()->email}}</h1>
            <p class="text-center font-11"><i class="bi bi-check-circle-fill color-green-dark pe-2"></i>Verified Account Holder</p>
            <div class="content mt-0 mb-2">
                <div class="list-group list-custom list-group-flush list-group-m rounded-xs">
                    <a href="#" class="list-group-item" data-bs-toggle="offcanvas" data-bs-target="#menu-information">
                        <i class="bi bi-person-circle"></i>
                        <div>Information</div>
                        <i class="bi bi-chevron-right"></i>
                    </a>
                    <a href="page-activity.html" class="list-group-item">
                        <i class="bi bi-bell-fill"></i>
                        <div>Notifications</div>
                        <span class="badge rounded-xl">3</span>
                    </a>
                    <a href="page-payments.html" class="list-group-item">
                        <i class="bi bi-credit-card"></i>
                        <div>Your Payments</div>
                        <i class="bi bi-chevron-right"></i>
                    </a>
                    <a href="page-reports.html" class="list-group-item">
                        <i class="bi bi-bar-chart-fill"></i>
                        <div>Your Statistics</div>
                        <i class="bi bi-chevron-right"></i>
                    </a>
                    <a href="#" class="list-group-item" data-bs-toggle="offcanvas" data-bs-target="#menu-highlights">
                        <i class="bi bi-droplet-fill"></i>
                        <div>Color Scheme</div>
                        <i class="bi bi-chevron-right"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="btn btn-full mx-3 gradient-highlight shadow-bg shadow-bg-xs">Contact Support</div>


    <div id="menu-information" class="offcanvas offcanvas-start" style="display: block; visibility: visible;" aria-modal="true" role="dialog">
        <div style="width:100vw">

            <div class="pt-3">
                <div class="page-title d-flex">
                    <div class="align-self-center">
                        <a href="#" data-bs-dismiss="offcanvas" class="me-3 ms-0 icon icon-xxs bg-theme rounded-s shadow-m">
                            <i class="bi bi-chevron-left color-theme font-14"></i>
                        </a>
                    </div>
                    <div class="align-self-center me-auto">
                        <h1 class="color-theme mb-0 font-18">Back to Profile</h1>
                    </div>
                    <div class="align-self-center ms-auto">
                        <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-sidebar" class="icon icon-xxs gradient-highlight color-white shadow-bg shadow-bg-xs rounded-s">
                            <i class="bi bi-list font-20"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="content mt-0">
                <h5 class="pb-3 pt-4">Personal Information</h5>
                <div class="form-custom form-label form-border mb-3 bg-transparent">
                    <input type="text" class="form-control rounded-xs" id="c1a" placeholder="nick.user.name">
                    <label for="c1a" class="form-label-always-active color-highlight">Username</label>
                    <span>(required)</span>
                </div>
                <div class="form-custom form-label form-border mb-3 bg-transparent">
                    <input type="text" class="form-control rounded-xs" id="c1ab" placeholder="John">
                    <label for="c1ab" class="form-label-always-active color-highlight">First Name</label>
                    <span>(required)</span>
                </div>
                <div class="form-custom form-label form-border mb-3 bg-transparent">
                    <input type="text" class="form-control rounded-xs" id="c1abc" placeholder="Doeson">
                    <label for="c1abc" class="form-label-always-active color-highlight">Last Name</label>
                    <span>(required)</span>
                </div>
                <div class="form-custom form-label form-border mb-3 bg-transparent">
                    <input type="text" class="form-control rounded-xs" id="c1abcd" placeholder="1 Apple Street, California, USA">
                    <label for="c1abcd" class="form-label-always-active color-highlight">Address</label>
                    <span>(required)</span>
                </div>
                <div class="form-custom form-label form-border mb-3 bg-transparent">
                    <input type="email" class="form-control rounded-xs" id="c1" placeholder="name@domain.com">
                    <label for="c1" class="color-highlight form-label-always-active">Email Address</label>
                    <span>(required)</span>
                </div>
                <h5 class="pb-3 pt-4">Default Settings</h5>
                <div class="form-custom form-label form-border form-icon">
                    <i class="bi bi-calendar font-13"></i>
                    <label for="c6a" class="color-highlight form-label-always-active">Default Account</label>
                    <select class="form-select rounded-xs" id="c6a">
                        <option value="0" selected="">Main Account</option>
                        <option value="01">Savings Account</option>
                        <option value="02">Company Account</option>
                    </select>
                </div>
                <h5 class="pb-3 pt-4">Account Security</h5>
                <div class="form-custom form-label form-border mb-3 bg-transparent">
                    <input type="tel" class="form-control rounded-xs" id="c21" value="+1 234 567 809">
                    <label for="c21" class="color-highlight form-label-always-active">Phone Number</label>
                    <span>(required)</span>
                </div>
                <div class="form-custom form-label form-border mb-3 bg-transparent">
                    <input type="password" class="form-control rounded-xs" id="c2" value="Old Password">
                    <label for="c2" class="color-highlight form-label-always-active">Current Password</label>
                    <span>(required)</span>
                </div>
                <div class="form-custom form-label form-border mb-4 bg-transparent">
                    <input type="password" class="form-control rounded-xs" id="c3" value="New Password">
                    <label for="c3" class="color-highlight form-label-always-active">New Password</label>
                    <span>(required)</span>
                </div>
                <a href="#" data-bs-dismiss="offcanvas" class="btn btn-full gradient-highlight shadow-bg shadow-bg-s mt-4">Apply Settings</a>
            </div>
        </div>
    </div>
@endsection
