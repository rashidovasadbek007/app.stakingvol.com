@extends('layouts.app')
@section('title', $pageTitle)
@section('content')
    <div class="card card-style">
        <div class="content">
            <h2 class="text-lg font-medium mr-auto">
                Deposit
            </h2>
            <p class="mb-0 font-11">Amount {{number_format(getAmount($data->amount))}} {{__($data->wallet->symbol)}}</p>
            <p class="mb-0 font-11">Charge {{number_format(getAmount($data->charge))}} {{__($data->wallet->symbol)}}</p>
            <p class="mb-0 font-11">Payable {{number_format(getAmount($data->amount + $data->charge))}} {{__($data->wallet->symbol)}}</p>
            <p class="mb-0 font-11">In  {{$data->baseCurrency()}}: {{number_format(getAmount($data->final_amo))}}</p>
            @if($data->wallet)
                <p class="mb-1 font-11">
                    Conversion with
                    <b> {{ __($data->wallet->symbol) }}</b>  and final value will Show on next step
                </p>
            @endif
            <a href="/deposit/manual" class="btn btn-danger px-3 py-2">Pay Now</a>

        </div>
    </div>
@endsection


