@extends('layouts.app')
@section('title', $pageTitle)

@section('content')
    <!--Account Activity Notification-->
    <div class="alert p-0 alert-dismissible fade show mb-n3" role="alert">
        <div class="card card-style gradient-green shadow-bg shadow-bg-s">
            <div class="content">
                <a href="page-activity.html" class="d-flex">
                    <div class="align-self-center">
                        <h1 class="mb-0 font-40"><i class="bi bi-check-circle color-white pe-3"></i></h1>
                    </div>
                    <div class="align-self-center">
                        <h5 class="color-white font-700 mb-0 mt-0">
                            Withdrawal of funds to your <br> Account has been successful.
                        </h5>
                    </div>
                    <div class="align-self-center ms-auto">
                        <span data-bs-dismiss="alert" class="icon-l"><i
                                class="bi bi-x font-20 pt-1 d-block color-white"></i></span>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="card card-style">
        <div class="content">
            <div class="tabs tabs-pill" id="tab-group-2">
                <div class="tab-controls rounded-m p-1 overflow-visible">
                    <a class="font-13 rounded-m shadow-bg shadow-bg-s" data-bs-toggle="collapse" href="#tab-4"
                       aria-expanded="true">Recent</a>
                    <a class="font-13 rounded-m shadow-bg shadow-bg-s" data-bs-toggle="collapse" href="#tab-5"
                       aria-expanded="false">Transfers</a>
                    <a class="font-13 rounded-m shadow-bg shadow-bg-s" data-bs-toggle="collapse" href="#tab-6"
                       aria-expanded="false">Payments</a>
                </div>
                <div class="mt-3"></div>
                <!-- Tab Group 1 -->
                <div class="collapse show" id="tab-4" data-bs-parent="#tab-group-2">
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-1">
                        <div class="align-self-center">
                                <span class="icon gradient-red me-2 shadow-bg shadow-bg-s rounded-s">
                                    <img src="/frontend/images/pictures/6s.jpg" width="45" class="rounded-s" alt="img">
                                </span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Karla Black</h5>
                            <p class="mb-0 font-11 opacity-70">12th March <span class="copyright-year"></span></p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-green-dark">$150.55</h4>
                            <p class="mb-0 font-11"> Received</p>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-2">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-brown shadow-bg shadow-bg-xs"><i
                                    class="bi bi-wallet color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Withdrawal</h5>
                            <p class="mb-0 font-11 opacity-70">12th March <span class="copyright-year"></span></p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-blue-dark">$345.31</h4>
                            <p class="mb-0 font-11">Main Account</p>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-3">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-orange shadow-bg shadow-bg-xs"><i
                                    class="bi bi-google color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Google Ads</h5>
                            <p class="mb-0 font-11 opacity-70">14th March <span class="copyright-year"></span></p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-red-dark">$324.55</h4>
                            <p class="mb-0 font-11">Bill Payment</p>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-4">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-green shadow-bg shadow-bg-xs"><i
                                    class="bi bi-person-circle font-18 color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Karla Black</h5>
                            <p class="mb-0 font-11 opacity-70">Awaiting Approval</p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <span class="btn btn-xxs gradient-green shadow-bg shadow-bg-xs">Details</span>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-5">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-blue shadow-bg shadow-bg-xs"><i
                                    class="bi bi-lock-fill font-18 color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Verification</h5>
                            <p class="mb-0 font-11 opacity-70">Action Required</p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <span class="btn btn-xxs gradient-blue shadow-bg shadow-bg-xs">Verify</span>
                        </div>
                    </a>
                </div>
                <!-- Tab Group 2 -->
                <div class="collapse" id="tab-5" data-bs-parent="#tab-group-2">
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-1">
                        <div class="align-self-center">
                                <span class="icon gradient-yellow me-2 shadow-bg shadow-bg-xs rounded-s">
                                    <img src="/frontend/images/pictures/21s.jpg" width="45" class="rounded-s" alt="img"></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Jane Doe</h5>
                            <p class="mb-0 font-11 opacity-70">12th March <span class="copyright-year"></span></p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-red-dark">$250.00</h4>
                            <p class="mb-0 font-11">Transfered</p>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-1">
                        <div class="align-self-center">
                                <span class="icon gradient-red me-2 shadow-bg shadow-bg-s rounded-s">
                                    <img src="/frontend/images/pictures/6s.jpg" width="45" class="rounded-s" alt="img"></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Karla Black</h5>
                            <p class="mb-0 font-11 opacity-70">12th March <span class="copyright-year"></span></p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-green-dark">$150.55</h4>
                            <p class="mb-0 font-11"> Received</p>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-2">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-brown shadow-bg shadow-bg-xs"><i
                                    class="bi bi-wallet color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Withdrawal</h5>
                            <p class="mb-0 font-11 opacity-70">12th March <span class="copyright-year"></span></p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-blue-dark">$345.31</h4>
                            <p class="mb-0 font-11">Main Account</p>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-4">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-green shadow-bg shadow-bg-xs"><i
                                    class="bi bi-person-circle font-18 color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Karla Black</h5>
                            <p class="mb-0 font-11 opacity-70">Awaiting Approval</p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <span class="btn btn-xxs bg-green-dark shadow-bg shadow-bg-xs">Details</span>
                        </div>
                    </a>
                </div>
                <!-- Tab Group 3 -->
                <div class="collapse" id="tab-6" data-bs-parent="#tab-group-2">
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-3">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-orange shadow-bg shadow-bg-xs"><i
                                    class="bi bi-google color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Google Ads</h5>
                            <p class="mb-0 font-11 opacity-70">14th March <span class="copyright-year"></span></p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-red-dark">$324.55</h4>
                            <p class="mb-0 font-11">Bill Payment</p>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-4">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-green shadow-bg shadow-bg-xs"><i
                                    class="bi bi-caret-up-fill color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Bitcoin</h5>
                            <p class="mb-0 font-11 opacity-70">13th March <span class="copyright-year"></span></p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-blue-dark">+0.315%</h4>
                            <p class="mb-0 font-11">Stock Update</p>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-2">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-yellow shadow-bg shadow-bg-xs"><i
                                    class="bi bi-pie-chart-fill color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Dividends</h5>
                            <p class="mb-0 font-11 opacity-70">14th March <span class="copyright-year"></span></p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <h4 class="pt-1 mb-n1 color-green-dark">$950.00</h4>
                            <p class="mb-0 font-11">Wire Transfer</p>
                        </div>
                    </a>
                    <div class="divider my-2 opacity-50"></div>
                    <a href="#" class="d-flex py-1" data-bs-toggle="offcanvas" data-bs-target="#menu-activity-5">
                        <div class="align-self-center">
                            <span class="icon rounded-s me-2 gradient-blue shadow-bg shadow-bg-xs"><i
                                    class="bi bi-lock-fill font-18 color-white"></i></span>
                        </div>
                        <div class="align-self-center ps-1">
                            <h5 class="pt-1 mb-n1">Verification</h5>
                            <p class="mb-0 font-11 opacity-70">Action Required</p>
                        </div>
                        <div class="align-self-center ms-auto text-end">
                            <span class="btn btn-xxs gradient-blue shadow-bg shadow-bg-xs">Verify</span>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
