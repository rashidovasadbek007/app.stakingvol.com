@extends('layouts.app')
@section('title', $pageTitle)

@section('content')


    @foreach($userstakings as $s)
    <div class="card card-style bg-5" style="height: 200px;  transform: scale(0.99);">
        <div class="card-top p-3">
            <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-card-more" class="icon icon-xxs bg-white color-black float-end"><i class="bi bi-three-dots font-18"></i></a>
        </div>
        <div class="card-center">
            <div class="bg-theme px-3 py-2 rounded-end d-inline-block">
                <h1 class="font-13 my-n1">
                    <a class="color-theme" data-bs-toggle="collapse" href="#balance3" aria-controls="balance2">Click for Balance</a>
                </h1>
                <div class="collapse" id="balance3"><h2 class="color-theme font-26">$ {{ number_format($s->staking->total_amount)}}</h2></div>
            </div>
        </div>
        <strong class="card-top no-click font-12 p-3 color-white font-monospace">{{$s->staking?->description}}</strong>
        <strong class="card-bottom no-click p-3 text-start color-white font-monospace">1234 5678 1234 5661</strong>
        <strong class="card-bottom no-click p-3 text-end color-white font-monospace">08 / 2025</strong>
        <div class="card-overlay bg-black opacity-50"></div>
    </div>
    @endforeach

@endsection
