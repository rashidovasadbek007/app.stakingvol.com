@extends('layouts.auth')
@section('title', "Login")

@section('content')


<div class="card card-style">
    <div class="content">
        <h1 class="mb-0 pt-2">Sign In</h1>
        <p>
            Sign in your PayApp Account
        </p>
        <form method="POST" action="{{ route('login') }}">
            @csrf
        <div class="form-custom form-label form-border form-icon mb-3 bg-transparent">
            <i class="bi bi-person-circle font-13"></i>
            <input type="text" class="form-control rounded-xs" id="c1" name="email" placeholder="Username"/>
            <label for="c1" class="color-theme">Username</label>
            <span>(required)</span>
        </div>
        <div class="form-custom form-label form-border form-icon mb-4 bg-transparent">
            <i class="bi bi-asterisk font-13"></i>
            <input type="password" class="form-control rounded-xs" name="password" id="c2" placeholder="Password"/>
            <label for="c2" class="color-theme">Password</label>
            <span>(required)</span>
        </div>
        <div class="form-check form-check-custom">
            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
            <label class="form-check-label font-12" for="remember">Remember this account</label>
            <i class="is-checked color-highlight font-13 bi bi-check-circle-fill"></i>
            <i class="is-unchecked color-highlight font-13 bi bi-circle"></i>
        </div>
            <button type="submit" class="btn btn-full gradient-highlight w-100 shadow-bg shadow-bg-s mt-4">
                SIGN IN
            </button>
        </form>
        <div class="row">
            <div class="col-6 text-start">
                <a href="{{ route('password.request') }}" class="font-11 color-theme opacity-40 pt-4 d-block">Forgot Password?</a>
            </div>
            <div class="col-6 text-end">
                <a href="/register" class="font-11 color-theme opacity-40 pt-4 d-block">Create Account</a>
            </div>
        </div>
    </div>


@endsection
