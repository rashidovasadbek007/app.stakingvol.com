@extends('layouts.auth')
@section('title', "Register")

@section('content')
{{--<div class="container">--}}
{{--    <div class="row justify-content-center">--}}
{{--        <div class="col-md-8">--}}
{{--            <div class="card">--}}
{{--                <div class="card-header">{{ __('Register') }}</div>--}}

{{--                <div class="card-body">--}}
{{--                    <form method="POST" action="{{ route('register') }}">--}}
{{--                        @csrf--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>--}}

{{--                                @error('name')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">--}}

{{--                                @error('email')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">--}}

{{--                                @error('password')--}}
{{--                                    <span class="invalid-feedback" role="alert">--}}
{{--                                        <strong>{{ $message }}</strong>--}}
{{--                                    </span>--}}
{{--                                @enderror--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-3">--}}
{{--                            <label for="password-confirm" class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>--}}

{{--                            <div class="col-md-6">--}}
{{--                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <div class="row mb-0">--}}
{{--                            <div class="col-md-6 offset-md-4">--}}
{{--                                <button type="submit" class="btn btn-primary">--}}
{{--                                    {{ __('Register') }}--}}
{{--                                </button>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="card card-style">
    <div class="content">
        <h1 class="mb-0 pt-2">Sign Up</h1>
        <p>
            Create a PayApp Account
        </p>
        <form method="POST" action="{{ route('register') }}">
            @csrf
        <div class="form-custom form-label form-border form-icon mb-3 bg-transparent">
            <i class="bi bi-person-circle font-13"></i>
            <input type="text" class="form-control rounded-xs" id="c1" name="email" placeholder="Email Address">
            <label for="c1" class="color-theme">Email Address</label>
            <span>(required)</span>
        </div>
        <div class="form-custom form-label form-border form-icon mb-3 bg-transparent">
            <i class="bi bi-asterisk font-13"></i>
            <input type="password" class="form-control rounded-xs" id="c2" name="password" placeholder="Password">
            <label for="c2" class="color-theme">Password</label>
            <span>(required)</span>
        </div>
        <div class="form-custom form-label form-border form-icon mb-4 bg-transparent">
            <i class="bi bi-asterisk font-13"></i>
            <input type="password" class="form-control rounded-xs" id="c3" name="password_confirmation" placeholder="Confirm Password">
            <label for="c3" class="color-theme">Confirm Password</label>
            <span>(required)</span>
        </div>
        <div class="form-check form-check-custom">
            <input class="form-check-input" type="checkbox" name="terms" value="{{ old('terms') ? 'checked' : '' }}" id="c2a">
            <label class="form-check-label font-12" for="c2a">I agree with the <a href="#">Terms and Conditions</a>.</label>
            <i class="is-checked color-highlight font-13 bi bi-check-circle-fill"></i>
            <i class="is-unchecked color-highlight font-13 bi bi-circle"></i>
        </div>
            <button type="submit" class="btn btn-full w-100 gradient-highlight shadow-bg shadow-bg-s mt-4">
                Create Account
            </button>
        </form>
        <div class="row">
            <div class="col-6 text-start">
                <a href="{{ route('password.request') }}" class="font-11 color-theme opacity-40 pt-4 d-block">Forgot Password?</a>
            </div>
            <div class="col-6 text-end">
                <a href="/login" class="font-11 color-theme opacity-40 pt-4 d-block">Sign In Account</a>
            </div>
        </div>
    </div>
</div>
@endsection
