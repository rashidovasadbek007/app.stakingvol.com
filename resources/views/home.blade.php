@extends('layouts.app')
@section('title', $pageTitle)

@section('content')

    <!-- Main Card Slider-->

{{--    @dd($userstakings)--}}

    <div class="splide single-slider slider-no-dots slider-no-arrows slider-visible" id="single-slider-1">
        <div class="splide__track">
            <div class="splide__list">
                @foreach($userstakings as $uS)
                <div class="splide__slide">
                    <div class="card card-style m-0 bg-5 shadow-card shadow-card-m" style="height:200px">
                        <div class="card-top p-3">
                            <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-card-more" class="icon icon-xxs bg-white color-black float-end"><i class="bi bi-three-dots font-18"></i></a>
                        </div>
                        <div class="card-center">
                            <div class="bg-theme px-3 py-2 rounded-end d-inline-block">
                                <h1 class="font-13 my-n1">
                                    <a class="color-theme" data-bs-toggle="collapse" href="#balance_{{$uS->id}}" aria-controls="balance2">Click for Balance</a>
                                </h1>
                                <div class="collapse" id="balance_{{$uS->id}}"><h2 class="color-theme font-26">$ {{ number_format($uS->staking->total_amount)}}</h2></div>
                            </div>
                        </div>
                        <strong class="card-top no-click font-12 p-3 color-white font-monospace text-capitalize">{{$uS->staking?->description}}</strong>
                        <strong class="card-bottom no-click p-3 text-start color-white font-monospace">1234 5678 1234 5661</strong>
                        <strong class="card-bottom no-click p-3 text-end color-white font-monospace">08 / 2025</strong>
                        <div class="card-overlay bg-black opacity-50"></div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

    <!-- Quick Actions -->
    <div class="content py-2">
        <div class="d-flex text-center">
            <div class="me-auto">
                <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-transfer" class="icon icon-xxl rounded-m bg-theme shadow-m"><i class="font-28 color-green-dark bi bi-arrow-up-circle"></i></a>
                <h6 class="font-13 opacity-80 font-500 mb-0 pt-2">Deposit</h6>
            </div>
            <div class="m-auto">
                <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-request" class="icon icon-xxl rounded-m bg-theme shadow-m"><i class="font-28 color-red-dark bi bi-arrow-down-circle"></i></a>
                <h6 class="font-13 opacity-80 font-500 mb-0 pt-2">Withdraw</h6>
            </div>
            <div data-bs-toggle="offcanvas" data-bs-target="#menu-exchange" class="m-auto">
                <a href="#" class="icon icon-xxl rounded-m bg-theme shadow-m"><i class="font-28 color-blue-dark bi bi-arrow-repeat"></i></a>
                <h6 class="font-13 opacity-80 font-500 mb-0 pt-2">Crypto Exchange</h6>
            </div>
            <div class="ms-auto">
                <a href="/payment-bill" class="icon icon-xxl rounded-m bg-theme shadow-m"><i class="font-28 color-brown-dark bi bi-filter-circle"></i></a>
                <h6 class="font-13 opacity-80 font-500 mb-0 pt-2">Bills</h6>
            </div>
        </div>
    </div>

    <!-- Recent Activity Title-->
    <div class="content my-0 mt-n2 px-1">
        <div class="d-flex">
            <div class="align-self-center">
                <h3 class="font-16 mb-2">Last Transactions</h3>
            </div>
            <div class="align-self-center ms-auto">
                <a href="" class="font-12 pt-1">View All</a>
            </div>
        </div>
    </div>

    <!-- Recent Activity Cards-->
    <div class="card card-style">
        <div class="content">
            @foreach($transactions as $t)
{{--                @dd($t->user_staking_date)--}}
            <a href="" class="d-flex py-1">
                <div class="align-self-center ps-1">
                    {{$t->userStaking->staking->symbol}}
                    <h5 class="pt-1 mb-n1">{{date('d M Y', strtotime($t->user_staking_date))}}</h5>
{{--                    <p class="mb-0 font-11 opacity-50">{{date('d M Y', strtotime($t->user_staking_date))}}</p>--}}
                </div>
                <div class="align-self-center ms-auto text-end">
                    <h4 class="pt-1 mb-n1 color-red-dark">$ {{number_format($t->amount,1)}}</h4>
                    <p class="mb-0 font-11">{{$t->userStaking?->staking?->description}} </p>
                </div>
            </a>
            <div class="divider my-2 opacity-50"></div>
            @endforeach
        </div>
    </div>

    <!-- Account Activity Title-->
    <div class="content my-0 mt-n2 px-1">
        <div class="d-flex">
            <div class="align-self-center">
                <h3 class="font-16 mb-2">Withdrawal</h3>
            </div>
            <div class="align-self-center ms-auto">
                <a href="/user/activity" class="font-12 pt-1">View All</a>
            </div>
        </div>
    </div>

    <!--Account Activity Notification-->
    <div class="card card-style gradient-green shadow-bg shadow-bg-s">
        <div class="content">
            <a href="/user/activity" class="d-flex">
                <div class="align-self-center">
                    <h1 class="mb-0 font-40"><i class="bi bi-check-circle color-white pe-3"></i></h1>
                </div>
                <div class="align-self-center">
                    <h5 class="color-white font-700 mb-0 mt-0 pt-1">
                        Withdrawal of funds to your <br> Account has been successful.
                    </h5>
                </div>
                <div class="align-self-center ms-auto">
                    <i class="bi bi-arrow-right-short color-white d-block pt-1 font-20 opacity-50"></i>
                </div>
            </a>
        </div>
    </div>

    <!-- Send Money Title-->
    <div class="content mb-0">
        <div class="d-flex">
            <div class="align-self-center">
                <h3 class="font-16 mb-2">Send Money</h3>
            </div>
            <div class="align-self-center ms-auto">
                <a href="page-payment-transfer.html" class="font-12 pt-1">View All</a>
            </div>
        </div>
    </div>

    <!-- Send Money Slider-->
    <div class="splide quad-slider slider-no-dots slider-no-arrows slider-visible text-center" id="double-slider-2">
        <div class="splide__track">
            <div class="splide__list">
                <div class="splide__slide">
                    <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-friends-transfer" data-card-height="60" class="card border-0  bg-1 shadow-card shadow-card-m rounded-m"></a>
                    <h6 class="pt-2 font-600">Johnatan</h6>
                </div>
                <div class="splide__slide">
                    <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-friends-transfer" data-card-height="60" class="card border-0  bg-6 shadow-card shadow-card-m rounded-m"></a>
                    <h6 class="pt-2 font-600">Alexandra</h6>
                </div>
                <div class="splide__slide">
                    <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-friends-transfer" data-card-height="60" class="card border-0 bg-3 shadow-card shadow-card-m rounded-m"></a>
                    <h6 class="pt-2 font-600">Juanita</h6>
                </div>
                <div class="splide__slide">
                    <a href="#" data-bs-toggle="offcanvas" data-bs-target="#menu-friends-transfer" data-card-height="60" class="card border-0 bg-9 shadow-card shadow-card-m rounded-m"></a>
                    <h6 class="pt-2 font-600">Danielle</h6>
                </div>
            </div>
        </div>
    </div>

@endsection
